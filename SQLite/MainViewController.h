//
//  MainViewController.h
//  SQLite
//
//  Created by Hemant Kumar on 10/05/16.
//  Copyright © 2016 hemant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *keyfield;
@property (strong, nonatomic) IBOutlet UITextField *valueField;
- (IBAction)submitBtnTapped:(id)sender;
- (IBAction)getBtnTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *resultLabel;

@end
