//
//  MainViewController.m
//  SQLite
//
//  Created by Hemant Kumar on 10/05/16.
//  Copyright © 2016 hemant. All rights reserved.
//

#import "MainViewController.h"
#import "DBManager.h"

@interface MainViewController () <UITextViewDelegate>

@property (nonatomic, strong) DBManager *dbManager;
@property (nonatomic, strong) dispatch_queue_t  concurrentQueue;
@property (nonatomic, strong) NSArray *result;
@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSString *value;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // DBManager
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"myDictionary.sql"];
    self.concurrentQueue = dispatch_queue_create("com.SQLite.query",
                                                 DISPATCH_QUEUE_CONCURRENT);
    
    [self addNumberKeypadAccessory:[self keyfield] titles:[NSArray arrayWithObjects:@"Cancel", @"Done", nil] selectors:[NSArray arrayWithObjects:[NSValue valueWithPointer:@selector(cancelKeyPad)], [NSValue valueWithPointer:@selector(doneWithKeyPad)], nil] target:self];
    [self addNumberKeypadAccessory:[self valueField] titles:[NSArray arrayWithObjects:@"Cancel", @"Done", nil] selectors:[NSArray arrayWithObjects:[NSValue valueWithPointer:@selector(cancelKeyPad)], [NSValue valueWithPointer:@selector(doneWithKeyPad)], nil] target:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.title = @"Smaple SQL";
    [self.resultLabel setHidden:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)cancelKeyPad
{
    if ([self.keyfield canResignFirstResponder])
        [self.keyfield resignFirstResponder];
    if ([self.valueField canResignFirstResponder])
        [self.valueField resignFirstResponder];
}

- (void)doneWithKeyPad
{
    if ([self.keyfield canResignFirstResponder])
        [self.keyfield resignFirstResponder];
    if ([self.valueField canResignFirstResponder])
        [self.valueField resignFirstResponder];
}

- (void) writeData {
    // Convert valueArray into data/bytes. value array can be anything like string, array or dictionary
    NSData *valueObj;
    if (self.key && self.value) {
        valueObj = [NSKeyedArchiver archivedDataWithRootObject:self.value];
    } else {
        NSLog(@"Key or value is nil.");
        return;
    }
    
    // Prepare the query string.
    NSString *query;
    if (valueObj) {
        query = [NSString stringWithFormat:@"insert into keyValue (key, value) values('%@', ?)", self.key];
    }
    
    dispatch_barrier_async(self.concurrentQueue, ^{
        if (query) {
            [self.dbManager writeQuery:[query UTF8String] withData:valueObj withKey:self.key];
        }
        
        // If the query was successfully executed then pop the view controller.
        if (self.dbManager.affectedRows != 0) {
            NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.resultLabel setHidden:NO];
                [self.resultLabel setText:[NSString stringWithFormat:@"Result : Successfully inserted"]];
            });
        }
        else{
            NSLog(@"Could not execute the query.");
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.resultLabel setHidden:NO];
                [self.resultLabel setText:[NSString stringWithFormat:@"Result : Failed to insert"]];
            });
        }
    });
}

-(void)readData{
    // Prepare the query string.
    NSString *query;
    if (self.key && [self.key length] > 0) {
        query= [NSString stringWithFormat:@"select value from keyValue where key = '%@'", self.key];
    }
    dispatch_async(self.concurrentQueue, ^{
        if (query) {
            NSArray *result = [[NSArray alloc] initWithArray:[self.dbManager readQuery:[query UTF8String]]];
            // If the query was successfully executed and returned result.
            if (result && [result count] > 0) {
                [self convertDataIntoObject:result];
            } else {
                NSLog(@"No key value pair found.");
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.resultLabel setHidden:NO];
                    [self.resultLabel setText:[NSString stringWithFormat:@"Result : No key-value found"]];
                });
            }
        }
    });
}

-(void) convertDataIntoObject:(NSArray *) result {
    NSData *value = [[result objectAtIndex:0] objectAtIndex:0];
    
    NSObject *finalValue = [NSKeyedUnarchiver unarchiveObjectWithData:value];
    
    self.result = [[NSArray alloc] initWithObjects:finalValue, nil];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.resultLabel setHidden:NO];
        [self.resultLabel setText:[NSString stringWithFormat:@"Result : %@",[self.result firstObject]]];
    });
}

-(void)addNumberKeypadAccessory:(UITextField *)numberTextField titles:(NSArray *)titles selectors:(NSArray *)selectors target:(id)target
{
    if (!titles || !selectors || [titles count] == 0 || [selectors count] == 0) {
        return;
    }
    
    if ([titles count] != [selectors count]) {
        return;
    }
    
    UIToolbar *numberToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40)];
    
    NSMutableArray *toolBarItems = [NSMutableArray new];
    
    uint32_t count = 0;
    for (NSString *title in titles) {
        [toolBarItems addObject:[[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:target action:[((NSValue *)[selectors objectAtIndex:count]) pointerValue]]];
        if (count == [selectors count] - 2) {
            [toolBarItems addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
        }
        count++;
    }
    numberToolbar.items = toolBarItems;
    numberTextField.inputAccessoryView = numberToolbar;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)submitBtnTapped:(id)sender {
    [self writeDataToDB];
}

- (IBAction)getBtnTapped:(id)sender {
    if ([self.keyfield canResignFirstResponder])
        [self.keyfield resignFirstResponder];
    if ([self.valueField canResignFirstResponder])
        [self.valueField resignFirstResponder];
    
    self.key = [self.keyfield text];
    
    if (self.key && [[self key] length] > 0) {
        [self readData];
    } else {
        [self.resultLabel setHidden:NO];
        [self.resultLabel setText:[NSString stringWithFormat:@"Key is Empty. Please enter Key."]];
    }

}

-(void) writeDataToDB {
    if ([self.keyfield canResignFirstResponder])
        [self.keyfield resignFirstResponder];
    if ([self.valueField canResignFirstResponder])
        [self.valueField resignFirstResponder];
    
    self.key = [self.keyfield text];
    self.value = [self.valueField text];
    
    if (self.key && [[self key] length] > 0 && self.value && [[self value] length] > 0) {
        [self writeData];
        [self.keyfield setText:@""];
        [self.valueField setText:@""];
    } else {
        [self.resultLabel setHidden:NO];
        [self.resultLabel setText:[NSString stringWithFormat:@"Key or Value is Empty"]];
    }
}
@end
